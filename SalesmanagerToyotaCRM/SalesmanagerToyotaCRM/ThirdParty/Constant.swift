//
//  Constant.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 30/05/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import Foundation
import UIKit

let appName = "SalesmanagerToyotaCRM"

let User_Defaults = UserDefaults.standard
let appdelegate   = UIApplication.shared.delegate as! AppDelegate

let themeColor   = UIColor.init(red: 63.0/255.0, green: 32.0/255.0, blue: 33.0/255.0, alpha: 1.0)
let textSelectedColour = UIColor.init(red: 16.0/255.0, green: 69.0/255.0, blue: 126.0/255.0, alpha: 1.0)
let textUnSelectedColour = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)


let screenWidth  = (UIScreen.main.bounds.width).description
let screenHeight = (UIScreen.main.bounds.height).description

let width = UIScreen.main.bounds.width
let height = UIScreen.main.bounds.height

let iphone5sWidth = "568.0"
let iphone8Width  = "375.0"
let iphone8Height = "667.0"

let iphone8PlusWidth  = "414.0"
let iphone8PlusHeight = "736.0"

let iphoneXRWidth  = "414.0"
let iphoneXRHeight = "896.0"

let iphoneXSWidth  = "375.0"
let iphoneXSHeight = "812.0"

let LeadTaskSeque = "lead_task_seque"
