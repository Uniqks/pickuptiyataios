//
//  SideMenuVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 30/05/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class SideMenuVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
  
    @IBOutlet weak var topViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var sideMenuTableView: UITableView!
    
    var menuArray : [String] = ["Dashboard","Profile","Leads & Task"]
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuTableView.tableFooterView = UIView()
    }
    override func viewWillLayoutSubviews() {
        if iphoneXRHeight == screenHeight || iphoneXSHeight == screenHeight {
            topViewHeightConstant.constant = 35.0
        }else {
            topViewHeightConstant.constant = 20.0
        }
    }
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        cell.selectionStyle = .none
        cell.menuLabel.text = menuArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
           //self.performSegue(withIdentifier: LeadTaskSeque, sender: self)
            let cc = self.storyboard?.instantiateViewController(withIdentifier: "LeadTaskVC") as! LeadTaskVC
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = cc
        }
    }
 
}

// MARK: - Custom UITableViewClass
class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var menuImageview: UIImageView!
    @IBOutlet weak var menuLabel: UILabel!
 
}
