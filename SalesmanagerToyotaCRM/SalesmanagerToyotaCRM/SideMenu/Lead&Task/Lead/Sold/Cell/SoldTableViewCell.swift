//
//  SoldTableViewCell.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 05/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class SoldTableViewCell: UITableViewCell {
    
    @IBOutlet weak var soldBackView: UIView!
    @IBOutlet weak var carLabel: UILabel!
    @IBOutlet weak var customerIDLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var invoiceDateLabel: UILabel!
    @IBOutlet weak var statusBackView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateBackView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        soldBackView.layer.borderWidth = 1.0
        soldBackView.layer.borderColor = UIColor.lightGray.cgColor
        soldBackView.layer.cornerRadius = 10.0
        
        statusBackView.layer.cornerRadius = statusBackView.frame.height/2
        dateBackView.layer.cornerRadius = dateBackView.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
