//
//  LTSoldVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 04/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class LTSoldVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var soldTableView: UITableView!
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        soldTableView.register(UINib(nibName: "SoldTableViewCell", bundle: nil), forCellReuseIdentifier: "SoldTableViewCell")


        // Do any additional setup after loading the view.
    }
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SoldTableViewCell", for: indexPath) as! SoldTableViewCell
        
        cell.selectionStyle = .none
        return cell
    }
    


}
