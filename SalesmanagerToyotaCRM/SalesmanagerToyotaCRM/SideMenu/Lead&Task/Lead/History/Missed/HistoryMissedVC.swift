//
//  HistoryMissedVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 05/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class HistoryMissedVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var missedTableView: UITableView!
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.missedTableView.register(UINib(nibName: "MissedTableViewCell", bundle: nil), forCellReuseIdentifier: "MissedTableViewCell")
    }
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MissedTableViewCell", for: indexPath) as! MissedTableViewCell
        cell.selectionStyle = .none
        return cell
    }

}
