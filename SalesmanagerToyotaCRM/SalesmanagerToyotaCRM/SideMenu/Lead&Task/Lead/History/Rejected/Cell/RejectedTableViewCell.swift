//
//  RejectedTableViewCell.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 05/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class RejectedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var rejectedBackView: UIView!
    @IBOutlet weak var customerIDLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var interestedLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reasonLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rejectedBackView.layer.borderWidth = 1.0
        rejectedBackView.layer.borderColor = UIColor.lightGray.cgColor
        rejectedBackView.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
