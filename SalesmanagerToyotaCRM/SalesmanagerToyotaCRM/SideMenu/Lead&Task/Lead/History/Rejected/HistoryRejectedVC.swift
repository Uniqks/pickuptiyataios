//
//  HistoryRejectedVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 05/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class HistoryRejectedVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var rejectedTableView: UITableView!
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.rejectedTableView.register(UINib(nibName: "RejectedTableViewCell", bundle: nil), forCellReuseIdentifier: "RejectedTableViewCell")
    }
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RejectedTableViewCell", for: indexPath) as! RejectedTableViewCell
        cell.selectionStyle = .none
        return cell
    }

}
