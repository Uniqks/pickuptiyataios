//
//  LTHistoryVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 04/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit
import PageMenu

class LTHistoryVC: UIViewController, CAPSPageMenuDelegate {
     var pageMenu : CAPSPageMenu?

    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        // MARK: - Scroll menu setup
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.isHidden = true
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
        let reassignedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HistoryReassignedVC") as! HistoryReassignedVC
        reassignedVC.title = "REASSIGNED"
        controllerArray.append(reassignedVC)
        
       
        let missedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HistoryMissedVC") as! HistoryMissedVC
        missedVC.title = "MISSED"
        controllerArray.append(missedVC)
        
              let rejectedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HistoryRejectedVC") as! HistoryRejectedVC
        rejectedVC.title = "REJECTED"
        controllerArray.append(rejectedVC)
        
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
           
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(textSelectedColour),
            .selectionIndicatorColor(textSelectedColour),
            .menuMargin(20.0),
            .menuHeight(60.0),
            .selectedMenuItemLabelColor(textSelectedColour),
            .unselectedMenuItemLabelColor(textUnSelectedColour),
            .menuItemFont(UIFont(name: "Montserrat-Medium", size: 15.0)!),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorRoundEdges(true),
            .selectionIndicatorHeight(2.0),
            .menuItemSeparatorPercentageHeight(0.1)
            
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height - 85.0), pageMenuOptions: parameters)
        
        // Optional delegate
        pageMenu!.delegate = self
        
        self.view.addSubview(pageMenu!.view)
        
    }
    func didMoveToPage(_ controller: UIViewController, index: Int) {
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
    }
}
