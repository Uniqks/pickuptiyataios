//
//  HistoryReassignedVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 05/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class HistoryReassignedVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    

    @IBOutlet weak var reassignedTableView: UITableView!
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reassignedTableView.register(UINib(nibName: "ReassignedTableViewCell", bundle: nil), forCellReuseIdentifier: "ReassignedTableViewCell")
    }
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReassignedTableViewCell", for: indexPath) as! ReassignedTableViewCell
        cell.selectionStyle = .none
        return cell
    }

}
