//
//  ReassignedTableViewCell.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 05/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class ReassignedTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var reassignedBackView: UIView!
    @IBOutlet weak var customerIDLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var interestedLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var activeBackview: UIView!
    @IBOutlet weak var overDueBackView: UIView!
    @IBOutlet weak var deletedBackView: UIView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        reassignedBackView.layer.borderWidth = 1.0
        reassignedBackView.layer.borderColor = UIColor.lightGray.cgColor
        reassignedBackView.layer.cornerRadius = 10.0
        
        followButton.layer.cornerRadius = followButton.frame.height/2
        
        activeBackview.layer.cornerRadius = activeBackview.frame.height/2
        overDueBackView.layer.cornerRadius = overDueBackView.frame.height/2
        deletedBackView.layer.cornerRadius = deletedBackView.frame.height/2
//         acceptButton.layer.cornerRadius = acceptButton.frame.height/2
//         rejectButton.layer.cornerRadius = rejectButton.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}

