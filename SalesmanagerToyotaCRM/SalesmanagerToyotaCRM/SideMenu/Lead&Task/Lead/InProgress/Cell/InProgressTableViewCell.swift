//
//  InProgressTableViewCell.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 04/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class InProgressTableViewCell: UITableViewCell {
    
    @IBOutlet weak var inProgressBackView: UIView!
    @IBOutlet weak var customerIDLabel: UILabel!
    @IBOutlet weak var negotiationButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var interstedInLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var activeBackView: UIView!
    @IBOutlet weak var activeLabel: UILabel!
    @IBOutlet weak var overDueBackView: UIView!
    @IBOutlet weak var overDueLabel: UILabel!
    @IBOutlet weak var deletedBackView: UIView!
    @IBOutlet weak var deletedLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        inProgressBackView.layer.borderWidth = 1.0
        inProgressBackView.layer.borderColor = UIColor.lightGray.cgColor
        inProgressBackView.layer.cornerRadius = 10.0
        
         negotiationButton.layer.cornerRadius = negotiationButton.frame.height/2
         activeBackView.layer.cornerRadius = activeBackView.frame.height/2
         overDueBackView.layer.cornerRadius = overDueBackView.frame.height/2
         deletedBackView.layer.cornerRadius = deletedBackView.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
