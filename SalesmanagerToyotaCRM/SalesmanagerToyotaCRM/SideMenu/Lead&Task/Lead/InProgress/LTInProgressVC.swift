//
//  LTInProgressVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 04/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class LTInProgressVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var progressTableView: UITableView!
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.progressTableView.register(UINib(nibName: "InProgressTableViewCell", bundle: nil), forCellReuseIdentifier: "InProgressTableViewCell")
    }
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InProgressTableViewCell", for: indexPath) as! InProgressTableViewCell
        cell.selectionStyle = .none
        return cell
    }
    


}
