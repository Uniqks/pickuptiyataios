//
//  LTLLeadVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 04/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class LTLLeadVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var leadTableView: UITableView!
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
         self.leadTableView.register(UINib(nibName: "LeadTableViewCell", bundle: nil), forCellReuseIdentifier: "LeadTableViewCell")
    }
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadTableViewCell", for: indexPath) as! LeadTableViewCell
        cell.selectionStyle = .none
        return cell
    }

}
