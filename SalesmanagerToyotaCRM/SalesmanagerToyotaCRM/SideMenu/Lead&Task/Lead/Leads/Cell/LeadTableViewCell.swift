//
//  LeadTableViewCell.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 04/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class LeadTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leadBackView: UIView!
    @IBOutlet weak var customerIDLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        leadBackView.layer.borderWidth = 1.0
        leadBackView.layer.borderColor = UIColor.lightGray.cgColor
        leadBackView.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
