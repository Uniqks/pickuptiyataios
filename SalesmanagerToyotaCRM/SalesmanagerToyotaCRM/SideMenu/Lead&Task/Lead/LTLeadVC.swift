//
//  LTLeadVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 04/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit
import PageMenu

class LTLeadVC: UIViewController {
    var pageMenu : CAPSPageMenu?

    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: - Scroll menu setup
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
        //let controller1 : LTLLeadVC = LTLLeadVC(nibName: "LTLLeadVC", bundle: nil)
        
        let leadsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LTLLeadVC") as! LTLLeadVC
        leadsVC.title = "Leads"
        controllerArray.append(leadsVC)
        
        let inProgressVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LTInProgressVC") as! LTInProgressVC
        inProgressVC.title = "InProgress"
        controllerArray.append(inProgressVC)
        
        let historyVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LTHistoryVC") as! LTHistoryVC
        historyVC.title = "History"
        controllerArray.append(historyVC)
        
        let soldVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LTSoldVC") as! LTSoldVC
        soldVC.title = "Sold"
        controllerArray.append(soldVC)
        
        //        let controller2 : RecentsTableViewController = RecentsTableViewController()
        //        controller2.title = "InProgress"
        //        controllerArray.append(controller2)
        //
        //        let controller3 : RecentsTableViewController = RecentsTableViewController()
        //        controller3.title = "History"
        //        controllerArray.append(controller3)
        //
        //        let controller4 : RecentsTableViewController = RecentsTableViewController()
        //        controller4.title = "Sold"
        //        controllerArray.append(controller4)
        
        
        //        for i in 0...10 {
        //             let controller3 : RecentsTableViewController = RecentsTableViewController()
        //            controller3.title = "contr\(i)"
        //            //            controller3.view.backgroundColor = getRandomColor()
        //            controllerArray.append(controller3)
        //        }
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .selectionIndicatorColor(textSelectedColour),
            .unselectedMenuItemLabelColor(textUnSelectedColour),
            .menuItemFont(UIFont(name: "Montserrat", size: 18.0)!),
            .menuHeight(60),
            .menuMargin(40.0),
            .selectionIndicatorHeight(2.0),
            .bottomMenuHairlineColor(UIColor.black),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .selectedMenuItemLabelColor(textSelectedColour)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height - 20), pageMenuOptions: parameters)
        
        self.view.addSubview(pageMenu!.view)
    }

}
