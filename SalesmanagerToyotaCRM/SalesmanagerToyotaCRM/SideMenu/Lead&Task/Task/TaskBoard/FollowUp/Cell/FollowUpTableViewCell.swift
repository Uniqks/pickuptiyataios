//
//  FollowUpTableViewCell.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 06/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class FollowUpTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var scheduleBackView: UIView!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var timeBackView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        scheduleBackView.layer.cornerRadius = scheduleBackView.frame.height/2
        timeBackView.layer.cornerRadius     = timeBackView.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
