//
//  TaskBoardFollowUpVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 06/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class TaskBoardFollowUpVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var followUpTableView: UITableView!
    @IBOutlet weak var activeLabelBackView: UIView!
    @IBOutlet weak var activeLabel: UILabel!
    
    @IBOutlet weak var overDueLabelBackView: UIView!
    @IBOutlet weak var overDueLabel: UILabel!
    
    @IBOutlet weak var doneLabelBackView: UIView!
    @IBOutlet weak var doneLabel: UILabel!
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        activeLabelBackView.layer.cornerRadius = activeLabelBackView.frame.size.width/2
        activeLabelBackView.clipsToBounds = true
        
        overDueLabelBackView.layer.cornerRadius = overDueLabelBackView.frame.size.width/2
        overDueLabelBackView.clipsToBounds = true
        
        doneLabelBackView.layer.cornerRadius = doneLabelBackView.frame.size.width/2
        doneLabelBackView.clipsToBounds = true

        self.followUpTableView.register(UINib(nibName: "FollowUpTableViewCell", bundle: nil), forCellReuseIdentifier: "FollowUpTableViewCell")
    }
    
    @IBAction func addButton(_ sender: UIButton) {
    }
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowUpTableViewCell", for: indexPath) as! FollowUpTableViewCell
        
        cell.selectionStyle = .none
        return cell
    }

}
