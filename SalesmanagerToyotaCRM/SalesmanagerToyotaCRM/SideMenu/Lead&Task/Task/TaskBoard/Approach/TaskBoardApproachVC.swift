//
//  TaskBoardApproachVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 06/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class TaskBoardApproachVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var approachTableView: UITableView!
    @IBOutlet weak var scheduleBackView: UIView!
    @IBOutlet weak var overDueBackView: UIView!
    @IBOutlet weak var doneBackView: UIView!
    @IBOutlet weak var deletedBackView: UIView!
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scheduleBackView.layer.cornerRadius = scheduleBackView.frame.height/2
        overDueBackView.layer.cornerRadius  = overDueBackView.frame.height/2
        doneBackView.layer.cornerRadius     = doneBackView.frame.height/2
        deletedBackView.layer.cornerRadius  = deletedBackView.frame.height/2
        self.approachTableView.register(UINib(nibName: "ApprorachTableViewCell", bundle: nil), forCellReuseIdentifier: "ApprorachTableViewCell")
    }
    
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ApprorachTableViewCell", for: indexPath) as! ApprorachTableViewCell
        
        cell.selectionStyle = .none
        return cell
    }

}
