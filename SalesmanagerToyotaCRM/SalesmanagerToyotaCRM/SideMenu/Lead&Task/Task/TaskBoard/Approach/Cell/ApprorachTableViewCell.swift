//
//  ApprorachTableViewCell.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 06/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class ApprorachTableViewCell: UITableViewCell {
    
    @IBOutlet weak var phoneImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var myResponseButton: UIButton!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var scheduleBackView: UIView!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var timeBackView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        myResponseButton.layer.cornerRadius = myResponseButton.frame.height/2
        scheduleBackView.layer.cornerRadius = scheduleBackView.frame.height/2
        timeBackView.layer.cornerRadius     = timeBackView.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
