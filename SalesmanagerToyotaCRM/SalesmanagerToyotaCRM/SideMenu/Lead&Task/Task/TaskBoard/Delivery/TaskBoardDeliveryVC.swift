//
//  TaskBoardDeliveryVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 06/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class TaskBoardDeliveryVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var activeLabelBackView: UIView!
    @IBOutlet weak var activeLabel: UILabel!
    @IBOutlet weak var overDueLabelBackView: UIView!
    @IBOutlet weak var overDueLabel: UILabel!
    @IBOutlet weak var deletedLabelBackView: UIView!
    @IBOutlet weak var deletedLabel: UILabel!
    
    @IBOutlet weak var deliveryTableView: UITableView!
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activeLabelBackView.layer.cornerRadius   = activeLabelBackView.frame.height/2
        overDueLabelBackView.layer.cornerRadius  = overDueLabelBackView.frame.height/2
        deletedLabelBackView.layer.cornerRadius  = deletedLabelBackView.frame.height/2
        self.deliveryTableView.register(UINib(nibName: "DeliveryTableViewCell", bundle: nil), forCellReuseIdentifier: "DeliveryTableViewCell")
    }
    
    @IBAction func addAction(_ sender: UIButton) {
    }
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryTableViewCell", for: indexPath) as! DeliveryTableViewCell
        cell.selectionStyle = .none
        return cell
    }

    
}
