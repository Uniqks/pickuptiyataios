//
//  TaskTaskBoardVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 06/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit
import PageMenu

class TaskTaskBoardVC: UIViewController, CAPSPageMenuDelegate {
    var pageMenu : CAPSPageMenu?

    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        // MARK: - Scroll menu setup
       
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
        let approachVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskBoardApproachVC") as! TaskBoardApproachVC
        approachVC.title = "APPROACH"
        controllerArray.append(approachVC)
        
        let followUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskBoardFollowUpVC") as! TaskBoardFollowUpVC
        followUpVC.title = "FOLLOW UP"
        controllerArray.append(followUpVC)
        
        let approvalVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskBoardApprovalVC") as! TaskBoardApprovalVC
        approvalVC.title = "APPROVAL"
        controllerArray.append(approvalVC)
        
        let deliveryVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskBoardDeliveryVC") as! TaskBoardDeliveryVC
        deliveryVC.title = "DELIVERY"
        controllerArray.append(deliveryVC)
        
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor.white),
            .selectionIndicatorColor(textSelectedColour),
            .menuMargin(20.0),
            .menuHeight(60.0),
            .selectedMenuItemLabelColor(textSelectedColour),
            .unselectedMenuItemLabelColor(textUnSelectedColour),
            .menuItemFont(UIFont(name: "Montserrat-Medium", size: 15.0)!),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorRoundEdges(true),
            .selectionIndicatorHeight(2.0),
            .menuItemSeparatorPercentageHeight(0.1)
            
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height - 85.0), pageMenuOptions: parameters)
        
        // Optional delegate
        pageMenu!.delegate = self
        
        self.view.addSubview(pageMenu!.view)
        
    }
    func didMoveToPage(_ controller: UIViewController, index: Int) {
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
    }
    


}
