//
//  TaskVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 06/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit
import PageMenu

class TaskVC: UIViewController {
     var pageMenu : CAPSPageMenu?

    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: - Scroll menu setup
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
        let taskBoardVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskTaskBoardVC") as! TaskTaskBoardVC
        taskBoardVC.title = "Task Board"
        controllerArray.append(taskBoardVC)
        
//        let infoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskInfoVC") as! TaskInfoVC
//        infoVC.title = "Info"
//        controllerArray.append(infoVC)
//        
//        let notesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskNotesVC") as! TaskNotesVC
//        notesVC.title = "Notes"
//        controllerArray.append(notesVC)
//        
//        let formsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskFormsVC") as! TaskFormsVC
//        formsVC.title = "Forms"
//        controllerArray.append(formsVC)
//        
//        let dealsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskDealsVC") as! TaskDealsVC
//        dealsVC.title = "Deals"
//        controllerArray.append(dealsVC)
//        
//        let activityVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskActivityVC") as! TaskActivityVC
//        activityVC.title = "Activity"
//        controllerArray.append(activityVC)
//        
//        let performanceVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskPerformanceVC") as! TaskPerformanceVC
//        performanceVC.title = "Performance"
//        controllerArray.append(performanceVC)
        
      
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .selectionIndicatorColor(textSelectedColour),
            .unselectedMenuItemLabelColor(textUnSelectedColour),
            .menuItemFont(UIFont(name: "HelveticaNeue", size: 18.0)!),
            .menuHeight(60),
            .menuMargin(40.0),
            .selectionIndicatorHeight(2.0),
            .bottomMenuHairlineColor(UIColor.black),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .selectedMenuItemLabelColor(textSelectedColour)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height - 20), pageMenuOptions: parameters)
        
        self.view.addSubview(pageMenu!.view)
    }
    


}
