//
//  LeadTaskVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 03/06/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit
import PageMenu
import SlideMenuControllerSwift

class LeadTaskVC: UIViewController, CAPSPageMenuDelegate {
    var pageMenu : CAPSPageMenu?
    @IBOutlet weak var topTitleViewHeightConstant: NSLayoutConstraint!
    
    

    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        // MARK: - Scroll menu setup
       
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
        let leadVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LTLeadVC") as! LTLeadVC
        leadVC.title = "Lead"
        controllerArray.append(leadVC)
        
        let taskVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TaskVC") as! TaskVC
        taskVC.title = "Task"
        controllerArray.append(taskVC)
        
//        let leadVC : LTLeadVC = LTLeadVC()
//        leadVC.title = "Lead"
//        controllerArray.append(leadVC)
//
//        let taskVC : TaskVC = TaskVC()
//        taskVC.title = "Task"
//        controllerArray.append(taskVC)
        
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(textSelectedColour),
            .selectionIndicatorColor(textSelectedColour),
            .menuMargin(20.0),
            .menuHeight(60.0),
            .selectedMenuItemLabelColor(textSelectedColour),
            .unselectedMenuItemLabelColor(textUnSelectedColour),
            .menuItemFont(UIFont(name: "Montserrat-Medium", size: 22.0)!),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorRoundEdges(true),
            .selectionIndicatorHeight(2.0),
            .menuItemSeparatorPercentageHeight(0.1)
            
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 85.0, width: self.view.frame.width, height: self.view.frame.height - 105.0), pageMenuOptions: parameters)
        
        // Optional delegate
        pageMenu!.delegate = self
        
        self.view.addSubview(pageMenu!.view)
        
    }
    override func viewWillLayoutSubviews() {
        if iphoneXRHeight == screenHeight || iphoneXSHeight == screenHeight {
            topTitleViewHeightConstant.constant = 85.0
        }else {
            topTitleViewHeightConstant.constant = 70.0
        }
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print("did move to page")
        
//                var color : UIColor = UIColor(red: 18.0/255.0, green: 150.0/255.0, blue: 225.0/255.0, alpha: 1.0)
//                var navColor : UIColor = UIColor(red: 17.0/255.0, green: 64.0/255.0, blue: 107.0/255.0, alpha: 1.0)
//
//                if index == 1 {
//                    color = UIColor.orange
//                    navColor = color
//                } else if index == 2 {
//                    color = UIColor.gray
//                    navColor = color
//                } else if index == 3 {
//                    color = UIColor.purple
//                    navColor = color
//                }
//
//        UIView.animate(withDuration: 0.5, animations: { () -> Void in
//                    self.navigationController!.navigationBar.barTintColor = navColor
//                }) { (completed) -> Void in
//                    print("did fade")
//                }
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        print("will move to page")
        
//                var color : UIColor = UIColor(red: 18.0/255.0, green: 150.0/255.0, blue: 225.0/255.0, alpha: 1.0)
//                var navColor : UIColor = UIColor(red: 17.0/255.0, green: 64.0/255.0, blue: 107.0/255.0, alpha: 1.0)
//
//                if index == 1 {
//                    color = UIColor.orange
//                    navColor = color
//                } else if index == 2 {
//                    color = UIColor.gray
//                    navColor = color
//                } else if index == 3 {
//                    color = UIColor.purple
//                    navColor = color
//                }
//
//        UIView.animate(withDuration: 0.5, animations: { () -> Void in
//                    self.navigationController!.navigationBar.barTintColor = navColor
//                }) { (completed) -> Void in
//                    print("did fade")
//                }
    }
    
    @IBAction func backAction(_ sender: Any) {
       // self.navigationController?.popViewController(animated: true)

        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        let SideMenuVc  = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        let slideMenuController = SlideMenuController(mainViewController: nextVC, leftMenuViewController: SideMenuVc)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = slideMenuController
    }
    
}
