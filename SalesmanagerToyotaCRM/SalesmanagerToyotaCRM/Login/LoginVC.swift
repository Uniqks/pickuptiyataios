//
//  LoginVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 30/05/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift


class LoginVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailFieldBackView: UIView!
    
    @IBOutlet weak var emailImageView: UIImageView!
    
    
    @IBOutlet weak var emailIcnBackView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordFieldBackView: UIView!
    @IBOutlet weak var passwordIcnBackView: UIView!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.layer.cornerRadius = loginButton.frame.height/2
        
          emailFieldBackView.layer.cornerRadius = emailFieldBackView.frame.height/2
         emailIcnBackView.layer.cornerRadius = emailIcnBackView.frame.height/2
          passwordFieldBackView.layer.cornerRadius = passwordFieldBackView.frame.height/2
          passwordIcnBackView.layer.cornerRadius = passwordIcnBackView.frame.height/2
        
//        //MENICN
//        let namSvgImgVar: SVGKImage = SVGKImage(named: "ic_vector_star")
//        emailImageView.image = namSvgImgVar.uiImage
        
    }
    
    
    // MARK: - Button Action
    @IBAction func forgotPasswordAction(_ sender: Any) {
    }
    
    
    @IBAction func loginAction(_ sender: Any) {
    
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
//        let homeNav = UINavigationController(rootViewController: nextVC)
//        homeNav.setNavigationBarHidden(true, animated: true)
        let SideMenuVc  = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
       let slideMenuController = SlideMenuController(mainViewController: nextVC, leftMenuViewController: SideMenuVc)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = slideMenuController
    }
}
