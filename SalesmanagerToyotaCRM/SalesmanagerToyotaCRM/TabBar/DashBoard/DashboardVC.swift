//
//  DashboardVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 30/05/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class DashboardVC: UIViewController {
    
    @IBOutlet weak var topViewHeightConstant: NSLayoutConstraint!
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        if iphoneXRHeight == screenHeight || iphoneXSHeight == screenHeight {
            topViewHeightConstant.constant = 85.0
        }else {
            topViewHeightConstant.constant = 70.0
        }
    }
    
    // MARK: - Button Action
    @IBAction func menuAction(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func buttonAction(_ sender: Any) {
         self.performSegue(withIdentifier: LeadTaskSeque, sender: self)
    }
    
}
