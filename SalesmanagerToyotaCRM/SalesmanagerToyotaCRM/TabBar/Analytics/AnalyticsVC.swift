//
//  AnalyticsVC.swift
//  SalesmanagerToyotaCRM
//
//  Created by Vinay Piplani on 30/05/19.
//  Copyright © 2019 Piplani. All rights reserved.
//

import UIKit

class AnalyticsVC: UIViewController {

    @IBOutlet weak var topViewHeightConstant: NSLayoutConstraint!
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        if iphoneXRHeight == screenHeight || iphoneXSHeight == screenHeight {
            topViewHeightConstant.constant = 85.0
        }else {
            topViewHeightConstant.constant = 70.0
        }
    }
    
    // MARK: - Button Action
    @IBAction func menuAction(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
   

}
